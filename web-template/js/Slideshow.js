function showSlide(slideIndex) {
    document.getElementById("slideImage").src =
            "img/" + slideShow.slides[slideIndex].imageFile;
    document.getElementById("slideCaption").innerHTML =
            slideShow.slides[slideIndex].caption;
}

function nextSlide() {
    ++currentSlide;
    currentSlide %= numSlides;
    showSlide(currentSlide);
}

function previousSlide() {
    if (currentSlide === 0) {
        currentSlide = numSlides - 1;
    } else {
        --currentSlide;
    }
    showSlide(currentSlide);
}

function play() {
    imgPlayPause.src = "img/Pause.png";
    imgPlayPause.alt = "Pause";
    btnPlayPause.onclick = pause;
    btnPrev.disabled = true;
    btnNext.disabled = true;
    playTimer = setInterval(playTransition, playInterval);
}

function playTransition() {
    nextSlide();
}

function pause() {
    clearInterval(playTimer);
    imgPlayPause.src = "img/Play.png";
    imgPlayPause.alt = "Play";
    btnPlayPause.onclick = play;
    btnPrev.disabled = false;
    btnNext.disabled = false;
}

var btnPrev = document.getElementById("btnPrev");
var btnNext = document.getElementById("btnNext");
var btnPlayPause = document.getElementById("btnPlayPause");
var imgPlayPause = document.getElementById("imgPlayPause");
var slideShow = JSON.parse(slideShowDesc);
var numSlides = slideShow.slides.length;
var currentSlide = 0;
var playTimer;
var playInterval = 3000;
document.title = slideShow.title;
document.getElementById("slideShowTitle").innerHTML = slideShow.title;
showSlide(currentSlide);