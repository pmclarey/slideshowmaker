package ssm.view;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.LABEL_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.CSS_CLASS_DIALOG_LABEL;
import static ssm.StartupConstants.ICON_DIALOG_NEW;
import static ssm.StartupConstants.PATH_ICONS;

/**
 * This class extends AbstractDialog and notifies user when a new slide show
 * has been successfully created.
 * 
 * @author Patrick Clarey
 */
public class NewSlideShowDialog extends AbstractDialog {
    
    // THESE GO IN THE TOP DIVISION
    Label message;
    ImageView img;
    
    // THIS GOES IN THE BOTTOM DIVISION
    Button btOK;
    
    /**
     * Constructor
     */
    public NewSlideShowDialog() {
        Image image = new Image("file:" + PATH_ICONS + ICON_DIALOG_NEW);
        img = new ImageView(image);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        message = new Label(props.getProperty(LABEL_NEW_SLIDE_SHOW));
        message.getStyleClass().add(CSS_CLASS_DIALOG_LABEL);
        btOK = new Button("OK");
        btOK.setOnAction(e -> {
           stage.hide();
        });
        top.getChildren().addAll(img, message);
        bottom.getChildren().add(btOK);
    }
}
