package ssm.view;

import java.net.URI;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import static ssm.StartupConstants.ICON_APPLICATION_ICON;
import static ssm.StartupConstants.PATH_ICONS;

/**
 *
 * @author Patrick Clarey
 */
public class SlideShowWebView {
    private final Stage stage;
    private final Scene scene;
    private final StackPane pane;
    private final WebView webView;
    private final WebEngine webEngine;

    public SlideShowWebView(URI uri) {
        webView = new WebView();
        webEngine = webView.getEngine();
        webEngine.load(uri.toString());
        pane = new StackPane(webView);
        scene = new Scene(pane);
        stage = new Stage();
        stage.setScene(scene);
        Image icon = new Image("file:" + PATH_ICONS + ICON_APPLICATION_ICON);
        stage.getIcons().add(icon);
        
        // GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	stage.setX(bounds.getMinX());
	stage.setY(bounds.getMinY());
	stage.setWidth(bounds.getWidth());
	stage.setHeight(bounds.getHeight());
    }
    
    public Stage getWindow() {
        return stage;
    }
 }