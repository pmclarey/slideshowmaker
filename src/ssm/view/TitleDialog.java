package ssm.view;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.LABEL_ENTER_TITLE;
import static ssm.StartupConstants.CSS_CLASS_DIALOG_LABEL;
import static ssm.StartupConstants.ICON_DIALOG_TITLE;
import static ssm.StartupConstants.PATH_ICONS;


/**
 * This class extends AbstractDialog and presents the user with a TextField
 * for changing the title of the slide show.
 * 
 * @author Patrick Clarey
 */
public class TitleDialog extends AbstractDialog {
    
    // THIS REPRESENTS THE SLIDE SHOW TITLE
    ImageView img;
    String title;
    
    // THESE GO IN THE TOP DIVISION
    Label prompt;
    
    // THESE GO IN THE BOTTOM DIVISION
    TextField tfTitle;
    Button btOK;
    
    /**
     * Constructor
     */
    public TitleDialog() {
        Image image = new Image("file:" + PATH_ICONS + ICON_DIALOG_TITLE);
        img = new ImageView(image);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        prompt = new Label(props.getProperty(LABEL_ENTER_TITLE));
        prompt.getStyleClass().add(CSS_CLASS_DIALOG_LABEL);
        tfTitle = new TextField();
        tfTitle.setOnAction(e -> btOKClick());
        btOK = new Button("OK");
        btOK.setOnAction(e -> btOKClick());
        top.getChildren().addAll(img, prompt);
        bottom.getChildren().addAll(tfTitle, btOK);
    }
    
    private void btOKClick() {
        title = tfTitle.getText();
        stage.close();
    }
    
    /**
     * Shows the title change window and waits for user to select new title
     * 
     * @param prevTitle previous title associated with the slide show
     * 
     * @return  new title associated with the slide show
     */
    public String getTitle(String prevTitle) {
        tfTitle.setText(prevTitle);
        stage.showAndWait();
        return title;
    }
}