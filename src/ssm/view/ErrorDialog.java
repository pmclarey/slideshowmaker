package ssm.view;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import static ssm.StartupConstants.CSS_CLASS_DIALOG_LABEL;
import static ssm.StartupConstants.ICON_ERROR;
import static ssm.StartupConstants.PATH_ICONS;

/**
 * This class extends AbstractDialog and provides dialog boxes specific
 * to error messages.
 * 
 * @author Patrick Clarey
 */
public class ErrorDialog extends AbstractDialog {
    
    ImageView img;          // ICON REPRESENTING ERROR MESSAGE
    Label lbErrorMessage;   // THE ERROR MESSAGE TO BE DISPLAYED
    Button btOK;            // BUTTON TO CLOSE DIALOG
    
    /**
     * Constructor
     * 
     * @param errorMessage The String representing the message to be displayed
     * to the user.
     */
    public ErrorDialog(String errorMessage) {
        Image image = new Image("file:" + PATH_ICONS + ICON_ERROR);
        img = new ImageView(image);
        lbErrorMessage = new Label(errorMessage);
        lbErrorMessage.getStyleClass().add(CSS_CLASS_DIALOG_LABEL);
        btOK = new Button("OK");
        btOK.setOnAction(e -> {
            btOKClick();
        });
        top.getChildren().addAll(img, lbErrorMessage);
        bottom.getChildren().add(btOK);
    }
    
    /**
     * This closes the error dialog box.
     */
    public void btOKClick() { 
        stage.close();
    }
}
