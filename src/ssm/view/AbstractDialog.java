package ssm.view;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static ssm.StartupConstants.CSS_CLASS_ROOT;
import static ssm.StartupConstants.CSS_ID_BOTTOM;
import static ssm.StartupConstants.CSS_ID_TOP;
import static ssm.StartupConstants.ICON_APPLICATION_ICON;
import static ssm.StartupConstants.PATH_CSS;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_DIALOG;

/**
 * This class provides a template to be used for all dialog boxes in this
 * application.
 * 
 * @author Patrick Clarey
 */
public abstract class AbstractDialog {
    
    // THIS IS THE DIALOG BOX WINDOW AND ITS SCENE GRAPH
    Stage stage;
    Scene scene;
    
    // THIS PANE IS THIS ROOT FOR THE SCENE GRAPH
    VBox root;
    
    // THESE SPLIT THE DIALOG INTO TWO SECTIONS
    HBox top;       // USED FOR MESSAGES
    HBox bottom;    // USED FOR CONTROLS: BUTTONS, COMBOBOXES
    
    // THESE CONSTANTS PROVIDE THE WIDTH AND HEIGHT FOR ALL DIALOG BOXES
    private final int DIALOG_WIDTH = 400;
    private final int DIALOG_HEIGHT = 200;
    
    /**
     * Constructor creates a Stage and an empty Scene graph to be used as a base
     * for all concrete dialog box classes.
     */
    public AbstractDialog() {
        top = new HBox();
        bottom = new HBox();
        root = new VBox(top, bottom);
        scene = new Scene(root, DIALOG_WIDTH, DIALOG_HEIGHT);
        
        // ADD STYLE SHEET AND STYLE IDs
        scene.getStylesheets().add(PATH_CSS + STYLE_SHEET_DIALOG);
        root.getStyleClass().add(CSS_CLASS_ROOT);
        top.setId(CSS_ID_TOP);
        bottom.setId(CSS_ID_BOTTOM);
        
        stage = new Stage(StageStyle.UTILITY);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setOnCloseRequest(e -> e.consume());
        Image icon = new Image("file:" + PATH_ICONS + ICON_APPLICATION_ICON);
        stage.getIcons().add(icon);
    }
    
    /**
     * This method is for getting the window of a descendent class from another
     * class
     * 
     * @return The Stage for an implementation of AbstractDialog 
     */
    public Stage getWindow() {
        return stage;
    }
}