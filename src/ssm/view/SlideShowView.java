package ssm.view;

import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT_VIEWER;
import static ssm.LanguagePropertyType.TOOLTIP_NEXT_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_PREVIOUS_SLIDE;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.PATH_ICONS;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This class represents a presentation view of a slide show
 * 
 * @author Patrick Clarey
 */
public class SlideShowView {
    
    // THE INDEX OF THE CURRENTLY PRESENTED SLIDE
    int currentSlideIndex;
    
    // THE SLIDE SHOW BEING PRESENTED
    SlideShowModel slideShow;
    
    // THE WINDOW AND SCENE GRAPH OF THE PRESENTATION VIEW
    Stage stage;
    Scene scene;
    
    // THE ROOT OF THE SCENE GRAPH
    VBox root;
    
    // THIS GOES AT THE TOP, DISPLAYS SLIDE SHOW TITLE
    HBox titleBox;
    Label title;
    
    // THIS GOES IN THE MIDDLE, DISPLAYS SLIDE IMAGE AND CAPTION
    VBox imageBox;
    ImageView slideImageView;
    HBox captionBox;
    Label caption;
    
    // THIS GOES AT THE BOTTOM, PRESENTS USER WITH BASIC CONTROLS
    HBox controlBox;
    Button btPrevious;
    Button btNext;
    Button btExit;
    
    /**
     * Constructor
     * 
     * @param slideShowToView the slide show to be presented
     */
    public SlideShowView(SlideShowModel slideShowToView) {
        currentSlideIndex = 0;
        slideShow = slideShowToView;
        title = new Label(slideShow.getTitle());
        title.setStyle("-fx-font-size: 4em; -fx-font-family: Georgia, serif;");
        titleBox = new HBox(title);
        titleBox.setStyle("-fx-alignment: center;");
        slideImageView = new ImageView();
        slideImageView.setFitHeight(600);
        slideImageView.setPreserveRatio(true);
        slideImageView.setSmooth(true);
        slideImageView.setCache(true);
        caption = new Label();
        caption.setStyle("-fx-font-size: 2em; -fx-font-family: Arial, sans-serif;");
        captionBox = new HBox(caption);
        captionBox.setAlignment(Pos.CENTER);
        imageBox = new VBox(slideImageView, captionBox);
        imageBox.setAlignment(Pos.CENTER);
        imageBox.setSpacing(5);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        Image imageExit = new Image("file:" + PATH_ICONS + ICON_EXIT);
        Image imagePrevious = new Image("file:" + PATH_ICONS + ICON_PREVIOUS);
        Image imageNext = new Image("file:" + PATH_ICONS + ICON_NEXT);
        btPrevious = new Button();
        btPrevious.setGraphic(new ImageView(imagePrevious));
        btPrevious.setTooltip
                (new Tooltip(props.getProperty(TOOLTIP_PREVIOUS_SLIDE.toString())));
        btPrevious.setOnAction(e -> {
            currentSlideIndex--;
            loadSlide();
        });
        btNext = new Button();
        btNext.setGraphic(new ImageView(imageNext));
        btNext.setTooltip
                (new Tooltip(props.getProperty(TOOLTIP_NEXT_SLIDE.toString())));
        btNext.setOnAction(e -> {
            currentSlideIndex++;
            loadSlide();
        });
        
        btExit = new Button();
        btExit.setGraphic(new ImageView(imageExit));
        btExit.setTooltip
                (new Tooltip(props.getProperty(TOOLTIP_EXIT_VIEWER.toString())));
        btExit.setOnAction(e -> {
            stage.hide();
        });
        
        controlBox = new HBox(10.0);
        controlBox.setAlignment(Pos.CENTER);
        controlBox.getChildren().addAll(btPrevious, btExit, btNext);
        
        
        root = new VBox(titleBox, imageBox, controlBox);
        root.setAlignment(Pos.CENTER);
        root.setSpacing(20.0);
        scene = new Scene(root);
        stage = new Stage(StageStyle.UNDECORATED);
        stage.setScene(scene);
    }
    
    /**
     * This returns the presentation window to the calling function.
     * 
     * @return Stage with presentation controls 
     */
    public Stage getWindow() {
        return stage;
    }
    
    private void loadSlide() {
        ObservableList<Slide> slides = slideShow.getSlides();
        Slide currentSlide = slides.get(currentSlideIndex);
        String imagePath = "file:" + currentSlide.getImagePath() +
                currentSlide.getImageFileName();
        Image slideImage = new Image(imagePath);
        slideImageView.setImage(slideImage);
        caption.setText(currentSlide.getCaption());
        btPrevious.setDisable((currentSlideIndex == 0));
        btNext.setDisable(currentSlideIndex == slides.size() - 1);
    }
    
    /**
     * This opens up the presentation window.
     */
    public void viewSlideShow() {
        loadSlide();
        stage.show();
        stage.setMaximized(true);
    }
}
