package ssm.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import ssm.Language;
import static ssm.StartupConstants.CSS_CLASS_DIALOG_LABEL;
import static ssm.StartupConstants.ICON_LANGUAGE;
import static ssm.StartupConstants.LABEL_LANGUAGE_DIALOG;
import static ssm.StartupConstants.PATH_ICONS;

/**
 * This class extends AbstractDialog and presents the user with a choice
 * of supported languages at startup.
 * 
 * @author Patrick Clarey
 */
public class LanguageDialog extends AbstractDialog {
    
    // THE LANGUAGE WITH WHICH TO INITIALIZE ALL UI ELEMENTS
    Language language;   
    
    // THESE GO IN THE TOP DIVISION
    Label prompt;
    ImageView img;
    
    // THESE GO IN THE BOTTOM DIVISION
    ChoiceBox cbLanguages;
    Button btOK;
    
    // A LIST OF SUPPORTED LANGUAGES THAT POPULATE THE CHOICE BOX
    ObservableList<String> languages;
    
    
    /**
     * Constructor
     */
    public LanguageDialog() {
        languages = FXCollections.observableArrayList();
        
        // ADD ALL SUPPORTED LANGUAGES TO CHOICE BOX
        for (Language supportedLanguage : Language.values()) {
            languages.add(supportedLanguage.toString());
        }
        
        Image image = new Image("file:" + PATH_ICONS + ICON_LANGUAGE);
        img = new ImageView(image);
        prompt = new Label(LABEL_LANGUAGE_DIALOG);
        prompt.getStyleClass().add(CSS_CLASS_DIALOG_LABEL);
        cbLanguages = new ChoiceBox(languages);
        cbLanguages.setValue(languages.get(0));
        btOK = new Button("OK");
        btOK.setOnAction(e -> btOKClick());
        top.getChildren().addAll(img, prompt);
        bottom.getChildren().addAll(cbLanguages, btOK);
    }
    
    private void btOKClick() {
        if (cbLanguages.getValue().equals(languages.get(0))) {
            language = Language.ENGLISH;
        }
        else if (cbLanguages.getValue().equals(languages.get(1))) {
            language = Language.DEUTSCH;
        }
        
        stage.close();
    }
    
    /**
     * Retrieves the user selected Language
     * 
     * @return Language for the application's elements 
     */
    public Language getLanguage() {
        stage.showAndWait();
        return language;
    }   
}