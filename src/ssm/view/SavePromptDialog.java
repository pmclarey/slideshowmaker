package ssm.view;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.LABEL_SAVE_PROMPT;
import static ssm.LanguagePropertyType.BUTTON_CANCEL;
import static ssm.LanguagePropertyType.BUTTON_DO_NOT_SAVE;
import static ssm.LanguagePropertyType.BUTTON_SAVE;
import static ssm.StartupConstants.CSS_CLASS_DIALOG_LABEL;
import static ssm.StartupConstants.ICON_DIALOG_QUESTION;
import static ssm.StartupConstants.PATH_ICONS;

/**
 * This class extends AbstractDialog and is presented to the user when they
 * are attempting to perform an operation that could result in the loss of work.
 * The window associated with this class asks the user if they would like to
 * save before continuing.
 * 
 * @author Patrick Clarey
 */
public class SavePromptDialog extends AbstractDialog {
    
    // Whether the user elected to save(or not) or simply cancel
    boolean save, cancel;
    
    // THESE GO IN THE TOP DIVISION
    ImageView img;
    Label lbSavePrompt;
    
    // THESE GO IN THE BOTTOM DIVISION
    Button btCancel;
    Button btDoNotSave;
    Button btSave;
    
    /**
     * Constructor
     */
    public SavePromptDialog() {
        save = true;
        cancel = false;
        Image image = new Image("file:" + PATH_ICONS + ICON_DIALOG_QUESTION);
        img = new ImageView(image);
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        lbSavePrompt = new Label(props.getProperty(LABEL_SAVE_PROMPT));
        lbSavePrompt.getStyleClass().add(CSS_CLASS_DIALOG_LABEL);
        btCancel = new Button(props.getProperty(BUTTON_CANCEL));
        btDoNotSave = new Button(props.getProperty(BUTTON_DO_NOT_SAVE));
        btSave = new Button(props.getProperty(BUTTON_SAVE));
        top.getChildren().addAll(img, lbSavePrompt);
        bottom.getChildren().addAll(btCancel, btDoNotSave, btSave);
        btCancel.setOnAction( e-> {
            save = false;
            cancel = true;
            stage.hide();
        });
        btDoNotSave.setOnAction(e -> {
            save = false;
            cancel = false;
            stage.hide();
        });
        btSave.setOnAction(e -> {
            save = true;
            cancel = false;
            stage.hide();
        });
    }
    
    /**
     * Returns whether the user wants to save his or her work.
     * 
     * @return true if the user elected to save, false otherwise
     */
    public boolean isToBeSaved() {
        return save;
    }
    
    /**
     * Returns whether the user elected to cancel the operation.
     * 
     * @return true if the user elected to cancel, false otherwise
     */
    public boolean isCancelled() {
        return cancel;
    }
}