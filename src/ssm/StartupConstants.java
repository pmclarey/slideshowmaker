package ssm;

/**
 * This class provides setup constants for initializing the application
 * that are NOT language dependent.
 * 
 * @author McKilla Gorilla & Patrick Clarey
 */
public class StartupConstants {

    // WE'LL LOAD ALL THE UI AND LANGUAGE PROPERTIES FROM FILES,
    // BUT WE'LL NEED THESE VALUES TO START THE PROCESS

    public static String PROPERTY_TYPES_LIST = "property_types.txt";
    public static String PROPERTIES_SCHEMA_FILE_NAME = "properties_schema.xsd";
    public static String PATH_DATA = "./data/";
    public static String PATH_SLIDE_SHOWS = PATH_DATA + "slide_shows/";
    public static String PATH_IMAGES = "./images/";
    public static String PATH_ICONS = PATH_IMAGES + "icons/";
    public static String PATH_SLIDE_SHOW_IMAGES = PATH_IMAGES + "slide_show_images/";
    public static String PATH_CSS = "/ssm/style/";
    public static String PATH_SITES = "./sites/";
    public static String PATH_WEB_TEMPLATE = "./web-template/";
    public static String DIRECTORY_CSS = "css/";
    public static String DIRECTORY_IMG = "img/";
    public static String DIRECTORY_JS = "js/";
    public static String INDEX_HTML = "index.html";
    public static String SLIDESHOW_STYLE_CSS = "slideshow_style.css";
    public static String SLIDESHOW_JS = "Slideshow.js";
    public static String SLIDESHOW_JS_DESC = "SlideShowDesc.js";
    public static String STYLE_SHEET_UI = "SlideShowMakerStyle.css";
    public static String STYLE_SHEET_DIALOG = "Dialog.css";

    // HERE ARE THE LANGUAGE INDEPENDENT GUI ICONS
    public static String ICON_NEW_SLIDE_SHOW = "New.png";
    public static String ICON_LOAD_SLIDE_SHOW = "Load.png";
    public static String ICON_SAVE_SLIDE_SHOW = "Save.png";
    public static String ICON_VIEW_SLIDE_SHOW = "View.png";
    public static String ICON_TITLE_SLIDE_SHOW = "Title.png";
    public static String ICON_EXIT = "Exit.png";
    public static String ICON_ADD_SLIDE = "Add.png";
    public static String ICON_REMOVE_SLIDE = "Remove.png";
    public static String ICON_MOVE_UP = "MoveUp.png";
    public static String ICON_MOVE_DOWN = "MoveDown.png";
    public static String ICON_PAUSE = "Pause.png";
    public static String ICON_PLAY = "Play.png";
    public static String ICON_PREVIOUS = "Previous.png";
    public static String ICON_NEXT = "Next.png";
    public static String ICON_ERROR = "dialog-error.png";
    public static String ICON_LANGUAGE = "education-languages.png";
    public static String ICON_DIALOG_NEW = "new-slide-show.png";
    public static String ICON_DIALOG_QUESTION = "dialog-question.png";
    public static String ICON_DIALOG_TITLE = "TitleOrig.png";
    public static String ICON_APPLICATION_ICON = "SlideShowMaker.png";

    // DEFAULT PROPERTIES FOR NEWLY CREATED SLIDES
    public static String    DEFAULT_SLIDE_IMAGE = "DefaultStartSlide.png";
    public static int	    DEFAULT_THUMBNAIL_WIDTH = 200;
    public static int	    DEFAULT_SLIDE_SHOW_HEIGHT = 500;
    
    // CSS STYLE SHEET CLASSES
    public static String    CSS_CLASS_SLIDE_SHOW_PANE = "slide_show_pane";
    public static String    CSS_CLASS_HORIZONTAL_FILE_TOOLBAR = "horizontal_file_toolbar";
    public static String    CSS_CLASS_SLIDE_SHOW_WORKSPACE = "slide_show_workspace";
    public static String    CSS_CLASS_SLIDE_EDITOR_PANE = "slide_editor_pane";
    public static String    CSS_CLASS_SLIDE_EDITOR_SCROLL_PANE = "slide_editor_scroll_pane";
    public static String    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON = "vertical_toolbar_button";
    public static String    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON = "horizontal_toolbar_button";
    public static String    CSS_CLASS_SLIDE_SHOW_EDIT_VBOX = "slide_show_edit_vbox";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW = "slide_edit_view";
    public static String    CSS_CLASS_SLIDE_EDIT_VIEW_SELECTED = "slide_edit_view_selected";
    public static String    CSS_CLASS_SLIDE_EDIT_CAPTION_VBOX = "slide_edit_caption_vbox";
    
    // CSS STYLE SHEET IDs AND CLASSES FOR DIALOG BOXES
    public static String    CSS_ID_TOP = "top";
    public static String    CSS_ID_BOTTOM = "bottom";
    public static String    CSS_CLASS_ROOT = "root";
    public static String    CSS_CLASS_DIALOG_LABEL = "dialog_label";
    
    // UI LABELS
    public static String    LABEL_SLIDE_SHOW_TITLE = "slide_show_title";
    public static String    LABEL_LANGUAGE_DIALOG = "Please select a language.";
}