package ssm.error;

import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.ERROR_PROPERTIES_FILE_LOADING;
import ssm.view.ErrorDialog;
import ssm.view.SlideShowMakerView;

/**
 * This class provides error messages to the user when the occur. Note
 * that error messages should be retrieved from language-dependent XML files
 * and should have custom messages that are different depending on
 * the type of error so as to be informative concerning what went wrong.
 * 
 * @author McKilla Gorilla & Patrick Clarey
 */
public class ErrorHandler {
    // APP UI
    private final SlideShowMakerView ui;
    
    // KEEP THE APP UI FOR LATER
    public ErrorHandler(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
     * This method provides all error feedback. It gets the feedback text,
     * which changes depending on the type of error, and presents it to
     * the user in a dialog box.
     * 
     * @param errorType Identifies the type of error that happened, which
     * allows us to get and display different text for different errors.
     */
    public void processError(LanguagePropertyType errorType) {
        String errorFeedbackText = "";
        
        if (errorType.equals(ERROR_PROPERTIES_FILE_LOADING)) {
            errorFeedbackText = 
                    "An error occurred while loading the language property file"
                    + ", and this application needs to close.";
        }
        else {
            // GET THE FEEDBACK TEXT
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            errorFeedbackText = props.getProperty(errorType);
        }

        // POP OPEN A DIALOG TO DISPLAY TO THE USER
        ErrorDialog errorDialog = new ErrorDialog(errorFeedbackText);
        Stage errorWindow = errorDialog.getWindow();
        errorWindow.showAndWait();
    }    
}