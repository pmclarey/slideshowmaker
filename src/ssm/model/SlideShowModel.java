package ssm.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import ssm.view.SlideEditView;
import ssm.view.SlideShowMakerView;

/**
 * This class manages all the data associated with a slide show.
 * 
 * @author McKilla Gorilla & Patrick Clarey
 */
public class SlideShowModel {
    SlideShowMakerView ui;
    String title;
    ObservableList<Slide> slides;
    Slide selectedSlide;
    
    public SlideShowModel(SlideShowMakerView initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }

    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<Slide> getSlides() {
	return slides;
    }
    
    public Slide getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    public int getSelectedIndex() {
        if (!isSlideSelected()) {
            return -1;
        }
        
        return slides.indexOf(selectedSlide);
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(Slide initSelectedSlide) {
        if (isSlideSelected()) {
            SlideEditView slideEditor = ui.getSlideEditView(selectedSlide);
            slideEditor.setSelected(false);
        }
	selectedSlide = initSelectedSlide;
        if (selectedSlide != null) {
            SlideEditView slideEditor = ui.getSlideEditView(selectedSlide);
            slideEditor.setSelected(true);
        }
        ui.reloadSlideShowPane(this);
        ui.updateToolbarControls();
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }

    // SERVICE METHODS
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
	selectedSlide = null;
    }

    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     * @param initCaption Caption for this slide to add
     */
    public void addSlide(   String initImageFileName,
			    String initImagePath,
                            String initCaption) {
	Slide slideToAdd = new Slide(initImageFileName, initImagePath, initCaption);
	slides.add(slideToAdd);
	selectedSlide = slideToAdd;
	ui.reloadSlideShowPane(this);
        ui.markAsEdited();
    }

    /**
     * Removes the selected slide from the slide show.
     */
    public void removeSlide() {
        if (isSlideSelected()) {
            int removeIndex = slides.indexOf(selectedSlide);
            slides.remove(removeIndex);
            setSelectedSlide(null);
            ui.reloadSlideShowPane(this);
            ui.markAsEdited();
        }
    }
    
    /**
     * Moves the selected slide one slot down.
     */
    public void moveDown() {
        int fromIndex = getSelectedIndex();
        
        if (fromIndex >= 0 && fromIndex < slides.size() - 1) {
            int toIndex = fromIndex + 1;
            slides.set(fromIndex, slides.get(toIndex));
            slides.set(toIndex, selectedSlide);
            ui.reloadSlideShowPane(this);
            ui.markAsEdited();
        }
    }
    
    /**
     * Moves the selected slide one slot up.
     */
    public void moveUp() {
        int fromIndex = getSelectedIndex();
        
        if (fromIndex > 0) {
            int toIndex = fromIndex - 1;
            slides.set(fromIndex, slides.get(toIndex));
            slides.set(toIndex, selectedSlide);
            ui.reloadSlideShowPane(this);
            ui.markAsEdited();
        }
    }
}