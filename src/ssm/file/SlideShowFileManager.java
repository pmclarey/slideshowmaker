package ssm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.*;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import static ssm.StartupConstants.DIRECTORY_CSS;
import static ssm.StartupConstants.DIRECTORY_IMG;
import static ssm.StartupConstants.DIRECTORY_JS;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PAUSE;
import static ssm.StartupConstants.ICON_PLAY;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.INDEX_HTML;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.SLIDESHOW_STYLE_CSS;
import static ssm.StartupConstants.SLIDESHOW_JS;
import static ssm.StartupConstants.PATH_SITES;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.StartupConstants.PATH_WEB_TEMPLATE;
import static ssm.StartupConstants.SLIDESHOW_JS_DESC;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 * This class uses the JSON standard to read and write slide show data files.
 * 
 * @author McKilla Gorilla & Patrick Clarey
 */
public class SlideShowFileManager {
    // JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_TITLE = "title";
    public static String JSON_SLIDES = "slides";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_CAPTION = "caption";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";

    /**
     * This method saves all the data associated with a slide show to
     * a JSON file.
     * 
     * @param slideShowToSave The course whose data we are saving.
     * 
     * @throws IOException Thrown when there are issues writing
     * to the JSON file.
     */
    public void saveSlideShow(SlideShowModel slideShowToSave) throws IOException {
        // BUILD THE FILE PATH
        String slideShowTitle = "" + slideShowToSave.getTitle();
        String jsonFilePath = PATH_SLIDE_SHOWS + SLASH + slideShowTitle + JSON_EXT;
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);  
           
        // BUILD THE SLIDES ARRAY
        JsonArray slidesJsonArray = makeSlidesJsonArray(slideShowToSave.getSlides());
        
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add(JSON_TITLE, slideShowToSave.getTitle())
                                    .add(JSON_SLIDES, slidesJsonArray)
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(courseJsonObject);
    }
    
    /**
     * This method loads the contents of a JSON file representing a slide show
     * into a SlideShowModel object.
     * 
     * @param slideShowToLoad The slide show to load
     * @param jsonFilePath The JSON file to load.
     * @throws IOException 
     */
    public void loadSlideShow(SlideShowModel slideShowToLoad, String jsonFilePath) throws IOException {
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(jsonFilePath);
        
        // NOW LOAD THE COURSE
	slideShowToLoad.reset();
        slideShowToLoad.setTitle(json.getString(JSON_TITLE));
        JsonArray jsonSlidesArray = json.getJsonArray(JSON_SLIDES);
        for (int i = 0; i < jsonSlidesArray.size(); i++) {
	    JsonObject slideJso = jsonSlidesArray.getJsonObject(i);
	    slideShowToLoad.addSlide(	slideJso.getString(JSON_IMAGE_FILE_NAME),
					slideJso.getString(JSON_IMAGE_PATH),
                                        slideJso.getString(JSON_CAPTION));
	}
    }

    /**
     * This method creates a web site directory for a given slide show
     * 
     * @param slideShowToView the slide show to export
     * @throws IOException
     */
    public void createWebSite(SlideShowModel slideShowToView) throws IOException {
        final ObservableList<Slide> slides = slideShowToView.getSlides();
        final String SLIDE_SHOW_TITLE = slideShowToView.getTitle();
        final String SITE_ROOT = PATH_SITES + SLIDE_SHOW_TITLE + SLASH;
        final Path PATH_CSS = Paths.get(SITE_ROOT + DIRECTORY_CSS);
        final Path PATH_IMG = Paths.get(SITE_ROOT + DIRECTORY_IMG);
        final Path PATH_JS = Paths.get(SITE_ROOT + DIRECTORY_JS);
        
        // CREATE DIRECTORY STRUCTURE FOR WEB SITE
        Files.createDirectories(PATH_CSS);
        Files.createDirectories(PATH_IMG);
        Files.createDirectories(PATH_JS);
        
        // COPY HTML
        Path source = Paths.get(PATH_WEB_TEMPLATE + INDEX_HTML);
        Path target = Paths.get(SITE_ROOT + INDEX_HTML);
        Files.copy(source, target, REPLACE_EXISTING);
        
        // COPY CSS
        source = Paths.get(PATH_WEB_TEMPLATE + DIRECTORY_CSS + SLIDESHOW_STYLE_CSS);
        target = Paths.get(SITE_ROOT + DIRECTORY_CSS + SLIDESHOW_STYLE_CSS);
        Files.copy(source, target, REPLACE_EXISTING);
        
        // COPY JAVASCRIPT
        source = Paths.get(PATH_WEB_TEMPLATE + DIRECTORY_JS + SLIDESHOW_JS);
        target = Paths.get(SITE_ROOT + DIRECTORY_JS + SLIDESHOW_JS);
        Files.copy(source, target, REPLACE_EXISTING);
        
        // COPY ICONS
        source = Paths.get(PATH_ICONS + ICON_PREVIOUS);
        target = Paths.get(SITE_ROOT + DIRECTORY_IMG + ICON_PREVIOUS);
        Files.copy(source, target, REPLACE_EXISTING);
        source = Paths.get(PATH_ICONS + ICON_NEXT);
        target = Paths.get(SITE_ROOT + DIRECTORY_IMG + ICON_NEXT);
        Files.copy(source, target, REPLACE_EXISTING);
        source = Paths.get(PATH_ICONS + ICON_PLAY);
        target = Paths.get(SITE_ROOT + DIRECTORY_IMG + ICON_PLAY);
        Files.copy(source, target, REPLACE_EXISTING);
        source = Paths.get(PATH_ICONS + ICON_PAUSE);
        target = Paths.get(SITE_ROOT + DIRECTORY_IMG + ICON_PAUSE);
        Files.copy(source, target, REPLACE_EXISTING);
        
        // CREATE JAVASCRIPT WITH JSON STRING
        File data = new File(SITE_ROOT + DIRECTORY_JS + SLIDESHOW_JS_DESC);
        PrintWriter writer = new PrintWriter(data);
        writer.printf("var slideShowDesc = '{ %ctitle%c: %c%s%c,' +%n", 
                '"', '"', '"', cleanString(SLIDE_SHOW_TITLE), '"');
        writer.printf("'%cslides%c: [' +%n", '"', '"');
        
        Slide slide;
        
        for (int index = 0; index < slides.size() - 1; index++) {
            slide = slides.get(index);
            writer.printf("'{%cimageFile%c: %c%s%c,' +%n"
                    , '"', '"', '"', slide.getImageFileName(), '"');
            writer.printf("'%ccaption%c: %c%s%c},' +%n"
                    ,'"', '"', '"', cleanString(slide.getCaption()), '"');
            source = Paths.get(slide.getImagePath() + slide.getImageFileName());
            target = Paths.get(SITE_ROOT + DIRECTORY_IMG + slide.getImageFileName());
            Files.copy(source, target, REPLACE_EXISTING);
        }
        
        slide = slides.get(slides.size() - 1);
        writer.printf("'{%cimageFile%c: %c%s%c,' +%n"
                    , '"', '"', '"', slide.getImageFileName(), '"');
        writer.printf("'%ccaption%c: %c%s%c}]}';"
                    ,'"', '"', '"', cleanString(slide.getCaption()), '"');
        source = Paths.get(slide.getImagePath() + slide.getImageFileName());
        target = Paths.get(SITE_ROOT + DIRECTORY_IMG + slide.getImageFileName());
        Files.copy(source, target, REPLACE_EXISTING);
        writer.close();
    }

    // AND HERE ARE THE PRIVATE HELPER METHODS TO HELP THE PUBLIC ONES
    
    private String cleanString(String str) {
        str = str.replace("\"", "\\\"");
        return str.replace("'", "\\'");
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
        }
        return items;
    }
    
    private JsonArray makeSlidesJsonArray(List<Slide> slides) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Slide slide : slides) {
	    JsonObject jso = makeSlideJsonObject(slide);
	    jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;        
    }
    
    private JsonObject makeSlideJsonObject(Slide slide) {
        JsonObject jso = Json.createObjectBuilder()
		.add(JSON_IMAGE_FILE_NAME, slide.getImageFileName())
		.add(JSON_IMAGE_PATH, slide.getImagePath())
                .add(JSON_CAPTION, slide.getCaption())
		.build();
	return jso;
    }
}