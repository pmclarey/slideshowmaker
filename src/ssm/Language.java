package ssm;

/**
 * This enumerated type provides constants representing all supported languages,
 * and provides a method to get that language's implementation file.
 * 
 * @author Patrick Clarey
 */
public enum Language {
    ENGLISH, DEUTSCH;
    
    /**
     * Provides a file name for supporting a particular language.
     * 
     * @return String containing the file name that provides language support.
     */
    public String getPropertiesFileName() {
        switch(this) {
            case ENGLISH:
                return "properties_EN.xml";
            case DEUTSCH:
                return "properties_DE.xml";
            default:
                return "properties_EN.xml";
        }
    }
    
    /**
     * Fetches String representation for a Language constant.
     * 
     * @return String containing language name 
     */
    @Override
    public String toString() {
        switch(this) {
            case ENGLISH:
                return "English";
            case DEUTSCH:
                return "Deutsch";
            default:
                return super.toString();
        }
    }
}