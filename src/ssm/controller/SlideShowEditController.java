package ssm.controller;

import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.DEFAULT_IMAGE_CAPTION;
import static ssm.StartupConstants.DEFAULT_SLIDE_IMAGE;
import static ssm.StartupConstants.PATH_SLIDE_SHOW_IMAGES;
import ssm.model.SlideShowModel;
import ssm.view.SlideShowMakerView;

/**
 * This controller provides responses for the slide show edit tool bar,
 * which allows the user to add, remove, and reorder slides.
 * 
 * @author McKilla Gorilla & Patrick Clarey
 */
public class SlideShowEditController {
    // APP UI
    private final SlideShowMakerView ui;
    
    /**
     * This constructor keeps the UI for later.
     * @param initUI Main user interface class for this application
     */
    public SlideShowEditController(SlideShowMakerView initUI) {
	ui = initUI;
    }
    
    /**
    * Provides a response for when the user wishes to add a new
    * slide to the slide show.
    */
    public void processAddSlideRequest() {
	SlideShowModel slideShow = ui.getSlideShow();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	slideShow.addSlide(DEFAULT_SLIDE_IMAGE, PATH_SLIDE_SHOW_IMAGES,
                props.getProperty(DEFAULT_IMAGE_CAPTION));
    }
    
    /**
     * Provides a response for when the user wishes to remove a slide from the
     * slide show.
     */
    public void processRemoveSlideRequest() {
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.removeSlide();
    }
   
    /**
     * Provides a response for when the user wishes to move a slide down one
     * slot in the slide show.
     */
    public void processMoveDownRequest() {
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.moveDown();
    }
   
   
    /**
     * Provides a response for when the user wishes to move a slide up one
     * slot in the slide show.
     */
    public void processMoveUpRequest() {
        SlideShowModel slideShow = ui.getSlideShow();
        slideShow.moveUp();
   }
}