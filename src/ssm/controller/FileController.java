package ssm.controller;

import java.io.File;
import java.io.IOException;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import static ssm.LanguagePropertyType.ERROR_SLIDE_SHOW_SAVING;
import static ssm.LanguagePropertyType.ERROR_SLIDE_SHOW_LOADING;
import static ssm.LanguagePropertyType.ERROR_WEB_SITE_CREATION;
import static ssm.StartupConstants.INDEX_HTML;
import static ssm.StartupConstants.PATH_SITES;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import ssm.view.NewSlideShowDialog;
import ssm.view.SavePromptDialog;
import ssm.view.SlideShowWebView;
import ssm.view.TitleDialog;

/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & Patrick Clarey
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;

    // THE APP UI
    private final SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private final SlideShowFileManager slideShowIO;

    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initUI The UI class for this application
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    /**
     * Getter method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
    
    /**
     * This method should be called when a slide show has been edited.  It marks
     * the slide show as not saved, triggering a save dialog when a user attempts
     * to exit, make a new slide show, or open an existing one.
     */
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();
		slideShow.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.reloadSlideShowPane(slideShow);
                ui.updateToolbarControls(saved);

                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                Stage newSlideShowWindow = new NewSlideShowDialog().getWindow();
                newSlideShowWindow.showAndWait();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_SLIDE_SHOW_SAVING);
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_SLIDE_SHOW_SAVING);
        }
    }

    /**
     * This method will save the current slide show to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     * 
     * @return true if the slide show was successfully saved, false otherwise.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
	    
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_SLIDE_SHOW_SAVING);
	    return false;
        }
    }
    
    /**
     * This method will export a slide show into a web site and open
     * a new window that previews the web content.
     */
    public void handleViewSlideShowRequest() {
        try {
            // GET THE SLIDE SHOW TO MAKE INTO WEBSITE AND VIEW
            SlideShowModel slideShowToView = ui.getSlideShow();
            
            // MAKE WEBSITE DIRECTORY STRUCTURE
            slideShowIO.createWebSite(slideShowToView);
            
            // GET WEBVIEW WINDOW AND DISPLAY
            String slideShowTitle = slideShowToView.getTitle();
            File indexHtml = new File
                    (PATH_SITES + slideShowTitle + "/" + INDEX_HTML);
            Stage webViewStage = 
                    new SlideShowWebView(indexHtml.toURI()).getWindow();
            webViewStage.setTitle(slideShowTitle);
            Stage primaryStage = ui.getWindow();
            webViewStage.setOnHidden((WindowEvent we) -> {
                primaryStage.show();
            });
            primaryStage.hide();
            webViewStage.show();
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_WEB_SITE_CREATION);
        }
    }
    
    /**
     * This method opens a dialog that allows a user to change the slide show
     * title.  If the user makes a change, the slide show is marked as edited.
     */
    public void handleTitleChangeRequest() {
        SlideShowModel slideShow = ui.getSlideShow();
        String prevTitle = ui.getSlideShow().getTitle();
        String newTitle = new TitleDialog().getTitle(prevTitle);
        
        if (!newTitle.equals(prevTitle)) {
            slideShow.setTitle(newTitle);
            markAsEdited();
        }
     }

     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_SLIDE_SHOW_SAVING);
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is returned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        SavePromptDialog savePrompt = new SavePromptDialog();
        Stage savePromptWindow = savePrompt.getWindow();
        savePromptWindow.showAndWait();
        boolean saveWork = savePrompt.isToBeSaved();
        boolean cancel = savePrompt.isCancelled();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork) {
            SlideShowModel slideShow = ui.getSlideShow();
            slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (cancel) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.reloadSlideShowPane(slideShowToLoad);
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(ERROR_SLIDE_SHOW_LOADING);
            }
        }
    }

    
}